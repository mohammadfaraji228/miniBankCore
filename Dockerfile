FROM openswoole/swoole:php8.1

# Set Working Directory
WORKDIR /var/www/html/miniBankCore

# Install system dependencies, PHP extensions and clean up
RUN apt-get update && apt-get install -y \
        cron \
        dnsutils \
        libpq-dev \
        libzip-dev \
        libxml2-dev \
        git \
        zlib1g-dev \
        unzip \
        python3 \
        dos2unix \
    && docker-php-ext-install pgsql pdo zip bcmath exif opcache pdo_pgsql soap pcntl

RUN pecl install swoole \
    && docker-php-ext-enable swoole

RUN pecl install redis \
    && docker-php-ext-enable redis

# Copy composer.json and install miniBankCore
COPY ./miniBankCore/composer.json /var/www/html/miniBankCore/
RUN composer install --prefer-dist --no-scripts

# Copy application code and other necessary files
COPY ./miniBankCore/ /var/www/html/miniBankCore/

# Set up Cron Jobs
COPY cronjob /etc/cron.d/cronjob
RUN chmod 0644 /etc/cron.d/cronjob \
    && crontab /etc/cron.d/cronjob \
    && touch /var/log/crontab.log

# Publish Laravel Swoole configurations, install Sentry, Swoole
RUN php artisan vendor:publish --tag=laravel-swoole \
    && composer require swooletw/laravel-swoole


# Supervisor Configuration
COPY ./supervisord.conf /etc/supervisor/
RUN mkdir /var/run/supervisor

# Entry Point
COPY ./entrypoint.sh /var/www/html/miniBankCore/
RUN dos2unix ./entrypoint.sh
ENTRYPOINT ["/bin/bash","./entrypoint.sh"]

