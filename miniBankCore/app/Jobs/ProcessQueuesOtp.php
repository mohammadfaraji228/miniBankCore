<?php

namespace App\Jobs;

use App\Repositories\OtpRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProcessQueuesOtp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $otp;
    protected $mobileNumber;

    public function __construct($otp, $mobileNumber)
    {
        $this->otp = $otp;
        $this->mobileNumber = $mobileNumber;
    }

    public function handle()
    {
        //region Lock
        $lockKeyFirst = 'TRADE_JOB_FIRST_'.$this->mobileNumber;
        $lockKeySecond = 'TRADE_JOB_SECOND_'.$this->mobileNumber;

        $lock = Cache::lock($lockKeyFirst, 20);

        if (!$lock->get()) {
            // Handling if the job is already running
            return;
        }
        if (Cache::get($lockKeySecond)) {
            return;
        }

        $lockvalue = Str::uuid()->getClockSeqHiAndReservedHex();

        Cache::set($lockKeySecond, $lockvalue, 20);

        if (Cache::get($lockKeySecond) != $lockvalue) {
            return;
        }


        try
        {
            /**
             * @var OtpRepository $otpService
             */
            $otpService = resolve(OtpRepository::class);
            $otpService->send($this->otp, $this->mobileNumber);
        }
        catch (\Exception $exception)
        {
            Log::error($exception);
        }
        finally
        {
            $lock->release();
            Cache::forget($lockKeySecond);
        }


    }
}
