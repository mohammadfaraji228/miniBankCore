<?php

namespace App\Manager;

class CardValidator
{
    private static CardValidator $instance;


    public static function getInstance(): CardValidator
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function validate($cardNumber): bool
    {
        $sum = 0;

        for ($i = 0; $i < strlen($cardNumber); $i++)
        {
            if (($i + 1) % 2 == 0)
            {
                $result = intval($cardNumber[$i]) * 1;
            }
            else
            {
                if (intval($cardNumber[$i]) * 2 > 9)
                {
                    $result = intval($cardNumber[$i]) * 2 - 9;
                }
                else
                {
                    $result = intval($cardNumber[$i]) * 2;
                }
            }
            $sum += $result;
        }

        if ($sum % 10 == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
