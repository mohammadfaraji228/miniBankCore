<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Date;

/**
 * @property string card_number
 * @property int amount
 * @property int transferId
 */
class Fee extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['card_number', 'amount', 'transferId'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

}
