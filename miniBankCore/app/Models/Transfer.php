<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Date;

/**
 * @property string sender_card_number
 * @property string receiver_card_number
 * @property int amount
 * @property int fee
 * @property Date transaction_date
 */
class Transfer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['sender_card_number', 'receiver_card_number', 'amount', 'fee', 'transaction_date'];

    protected $hidden = ['created_at','updated_at','deleted_at'];

}
