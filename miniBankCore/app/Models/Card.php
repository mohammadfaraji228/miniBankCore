<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Date;

/**
 * @property int account_id
 * @property string card_number
 * @property Date expiration_date
 * @property string cvv2
 */
class Card extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'account_id',
        'card_number',
        'expiration_date',
        'cvv2',
    ];

    protected $hidden = ['cvv2','created_at','updated_at','deleted_at'];


    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transfer::class, 'sender_card_number', 'card_number')
            ->orWhere('receiver_card_number', 'card_number');
    }

}
