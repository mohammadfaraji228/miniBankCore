<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Date;

/**
 * @property string sender_card_number
 * @property string receiver_card_number
 * @property int amount
 * @property int fee
 * @property Date transaction_date
 */
class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['card_number', 'amount', 'transferId'];

    protected $hidden = ['created_at','updated_at'];

}
