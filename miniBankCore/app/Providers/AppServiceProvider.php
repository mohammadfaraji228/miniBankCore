<?php

namespace App\Providers;

use App\Repositories\AccountRepository;
use App\Repositories\CardRepository;
use App\Repositories\FeeRepository;
use App\Repositories\OtpRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\TransferRepository;
use App\Repositories\UserRepository;
use App\Services\AccountService;
use App\Services\CardService;
use App\Services\FeeService;
use App\Services\GasedakOtpService;
use App\Services\KaveNegarOtpService;
use App\Services\TransactionService;
use App\Services\TransferService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(UserRepository::class, UserService::class);
        $this->app->singleton(AccountRepository::class, AccountService::class);
        $this->app->singleton(CardRepository::class, CardService::class);
        $this->app->singleton(TransactionRepository::class, TransactionService::class);
        $this->app->singleton(TransferRepository::class, TransferService::class);
        $this->app->singleton(FeeRepository::class, FeeService::class);


        if (config('provider.otp.name') == 'kaveNagar')
        {
            $this->app->singleton(OtpRepository::class, KaveNegarOtpService::class);
        }
        elseif (config('provider.otp.name') == 'gasedak')
        {
            $this->app->singleton(OtpRepository::class, GasedakOtpService::class);
        }


    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
