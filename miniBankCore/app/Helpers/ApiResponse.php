<?php

namespace App\Helpers;

class ApiResponse
{
    public static function success($data, $message = 'Success', $code = 200)
    {
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => $message,
        ], $code);
    }

    public static function error($message, $code = 500)
    {
        return response()->json([
            'status' => false,
            'data' => [],
            'message' => $message,
        ], $code);
    }
}
