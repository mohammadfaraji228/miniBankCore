<?php

namespace App\Services;

use App\Models\Transaction;
use App\Repositories\TransactionRepository;

class TransactionService implements TransactionRepository
{
    /**
     * @inheritDoc
     */
    public function createTransaction($cardNumber, $amount, $transferId)
    {
        return Transaction::query()->create(['card_number' => $cardNumber,'amount' => $amount,'transferId' => $transferId]);
    }
}
