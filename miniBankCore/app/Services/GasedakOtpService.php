<?php

namespace App\Services;

use App\Repositories\OtpRepository;
use Illuminate\Support\Facades\Cache;

class GasedakOtpService implements OtpRepository
{

    /**
     * @inheritDoc
     */
    public function sendOtp($cardNumber, $amount, $mobileNumber)
    {
        // TODO: Implement sendOtp() method.
    }

    /**
     * @inheritDoc
     */
    public function validateOtp($cardNumber, $amount, $otp)
    {
        $key = 'OPT|' . $cardNumber . '|' . $amount . '|' . $amount;
        $savedOtp = Cache::get($key);
        if ($savedOtp)
        {
            if ($otp != $savedOtp)
            {
                return false;
            }
        }
        Cache::delete($key);
        return true;
    }

    public function send($otp, $mobileNumber)
    {
        // TODO: Implement send() method.
    }
}
