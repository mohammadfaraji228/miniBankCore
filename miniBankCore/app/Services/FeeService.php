<?php

namespace App\Services;

use App\Models\Fee;
use App\Models\Transfer;
use App\Repositories\FeeRepository;

class FeeService implements FeeRepository
{

    /**
     * @inheritDoc
     */
    public function createFeeByTransfer(Transfer $transfer)
    {
        $data = [
            'card_number' => $transfer->sender_card_number,
            'amount' => $transfer->fee,
            'transferId' => $transfer->id
        ];
        Fee::query()->create($data);
    }
}
