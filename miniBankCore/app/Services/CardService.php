<?php

namespace App\Services;

use App\Models\Card;
use App\Repositories\CardRepository;

class CardService implements CardRepository
{

    /**
     * @inheritDoc
     */
    public function getCardWithId($CardId)
    {
        // TODO: Implement getCardWithId() method.
    }

    /**
     * @inheritDoc
     */
    public function getCardWithCardNumber($cardNumber)
    {
        return Card::query()->where('card_number', $cardNumber)->first();
    }
}
