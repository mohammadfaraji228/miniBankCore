<?php

namespace App\Services;

use App\Jobs\ProcessQueuesOtp;
use App\Repositories\OtpRepository;
use Illuminate\Support\Facades\Cache;

class KaveNegarOtpService implements OtpRepository
{

    /**
     * @inheritDoc
     */
    public function sendOtp($cardNumber, $amount, $mobileNumber)
    {
        $key = 'OPT|' . $cardNumber . '|' . $amount . '|' . $amount;
        $otp = rand(100000,999999);
        Cache::set($key, $otp, 120);
        ProcessQueuesOtp::dispatch($otp, $mobileNumber)->onQueue('OTP');

    }

    /**
     * @inheritDoc
     */
    public function validateOtp($cardNumber, $amount, $otp)
    {
        $key = 'OPT|' . $cardNumber . '|' . $amount . '|' . $amount;
        $savedOtp = Cache::get($key);
        if ($savedOtp)
        {
            if ($otp != $savedOtp)
            {
                return false;
            }
        }
        Cache::delete($key);
        return true;
    }

    /**
     * @inheritDoc
     */
    public function send($otp, $mobileNumber)
    {
        // TODO: Implement send() method.
    }
}
