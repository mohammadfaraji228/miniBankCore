<?php

namespace App\Services;

use App\Models\Card;
use App\Models\Transfer;
use App\Repositories\CardRepository;
use App\Repositories\FeeRepository;
use App\Repositories\OtpRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\TransferRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransferService implements TransferRepository
{

    protected $cardService;
    protected $transactionService;
    protected $feeService;
    protected $otpService;

    public function __construct(CardRepository $cardRepository, TransactionRepository $transactionRepository, FeeRepository $feeRepository, OtpRepository $otpRepository)
    {
        $this->cardService = $cardRepository;
        $this->transactionService = $transactionRepository;
        $this->feeService = $feeRepository;
        $this->otpService = $otpRepository;
    }

    /**
     * @inheritDoc
     */
    public function transfer(array $data)
    {
        $sender = $data['sender_card_number'];
        $receiver = $data['receiver_card_number'];
        $amount = $data['amount'];
        $fee = 5000;
        $otp = $data['otp'];

        $checkOtp = $this->otpService->validateOtp($sender, $amount, $otp);
        if ($checkOtp)
        {
            return 'otp is not valid';
        }

        DB::beginTransaction();
        try {
            /** @var Card $senderCard */
            $senderCard = $this->cardService->getCardWithCardNumber($sender);
            $senderBalance = $senderCard->account()->balance;
            if ($senderBalance < $amount + $fee) {
                return 'insufficient balance';
            }
            /** @var Card $receiverCard */
            $receiverCard = $this->cardService->getCardWithCardNumber($receiver);

            $transfer = $this->create($data);

            $this->transactionService->createTransaction($senderCard->card_number, $amount * -1, $transfer->id);
            $this->transactionService->createTransaction($senderCard->card_number, $fee, $transfer->id);
            $this->transactionService->createTransaction($receiverCard->card_number, $amount, $transfer->id);

            $this->feeService->createFeeByTransfer($transfer);

            $senderCard->account()->decrement('balance', $amount + $fee);
            $receiverCard->account()->increment('balance', $amount);

            DB::commit();

        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error($exception->getMessage());
            return 'Transaction failed';
        }
        return 'Transfer successful';
    }

    /**
     * @inheritDoc
     */
    public function create(array $data)
    {
        return Transfer::query()->create([$data]);
    }

    /**
     * @inheritDoc
     */
    public function get($cardId)
    {
        return Transfer::query()->where('sender_card_number',$cardId)->orWhere('receiver_card_number',$cardId)->get();
    }

    /**
     * @inheritDoc
     */
    public function getLastTransfers($count)
    {
        return Transfer::query()->limit(10)->orderBy('created_at','desc')->get();
    }
}
