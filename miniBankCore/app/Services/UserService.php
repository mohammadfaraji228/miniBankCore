<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class UserService implements UserRepository
{

    /**
     * @return Collection|mixed
     */
    public function getAll()
    {
        return User::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return User::find($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return User::create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function update($id, array $data)
    {
        $user = User::find($id);
        if (!$user) {
            throw new Exception("User not found.");
        }

        $user->update($data);
        return $user;
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function delete($id)
    {
        $user = User::find($id);
        if (!$user) {
            throw new Exception("User not found.");
        }

        $user->delete();
        return $user;
    }

    public function checkUserExistence($email)
    {
        $user = User::query()->where('email', $email)->first();
        if ($user) {
            return true;
        } else {
            return false;
        }
    }
}
