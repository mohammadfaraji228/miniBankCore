<?php

namespace App\Repositories;

interface OtpRepository
{
    /**
     * @param $cardNumber
     * @param $amount
     * @return mixed
     */
    public function sendOtp($cardNumber, $amount, $mobileNumber);

    /**
     * @param $cardNumber
     * @param $amount
     * @param $otp
     * @return mixed
     */
    public function validateOtp($cardNumber, $amount, $otp);

    /**
     * @param $cardNumber
     * @param $amount
     * @param $mobileNumber
     * @return mixed
     */
    public function send($otp, $mobileNumber);
}
