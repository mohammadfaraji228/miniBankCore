<?php

namespace App\Repositories;

use App\Models\Transfer;

interface FeeRepository
{
    /**
     * @param Transfer $transfer
     * @return mixed
     */
    public function createFeeByTransfer(Transfer $transfer);
}
