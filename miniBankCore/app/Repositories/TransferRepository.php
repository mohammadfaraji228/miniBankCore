<?php

namespace App\Repositories;

interface TransferRepository
{
    /**
     * @param array $data
     * @return mixed
     */
    public function transfer(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param $cardId
     * @return mixed
     */
    public function get($cardId);

    /**
     * @param $count
     * @return mixed
     */
    public function getLastTransfers($count);
}
