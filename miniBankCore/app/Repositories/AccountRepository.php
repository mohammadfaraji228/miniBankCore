<?php

namespace App\Repositories;

interface AccountRepository
{
    /**
     * @param $mobileNumber
     * @return mixed
     */
    public function getAccountByMobile(string $mobileNumber);

    /**
     * @param $cardNumber
     * @return mixed
     */
    public function getAccountByCardNumber(string $cardNumber);
}
