<?php

namespace App\Repositories;

interface TransactionRepository
{
    /**
     * @param $cardNumber
     * @param $amount
     * @return mixed
     */
    public function createTransaction($cardNumber, $amount, $transferId);
}
