<?php

namespace App\Repositories;

use App\Models\Card;

interface CardRepository
{
    /**
     * @param $CardId
     * @return mixed
     */
    public function getCardWithId(int $CardId);

    /**
     * @param $cardNumber
     * @return Card
     */
    public function getCardWithCardNumber(string $cardNumber);
}
