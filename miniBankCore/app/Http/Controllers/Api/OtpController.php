<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Manager\CardValidator;
use App\Repositories\CardRepository;
use App\Repositories\OtpRepository;
use App\Repositories\TransferRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OtpController extends Controller
{

    protected $otpService;
    protected $cardService;

    public function __construct(OtpRepository $otpRepository, CardRepository $cardRepository)
    {
        $this->otpService = $otpRepository;
        $this->cardService = $cardRepository;
    }

    public function sendOtp(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'sender' => 'required|string',
            'amount' => 'required|integer',
        ]);
        $data['sender'] = convertNumber($data['sender']);

        if ($validator->fails()) {
            return ApiResponse::error($validator->errors(), 400);
        }

        $senderCardValidator = CardValidator::getInstance()->validate($data['sender']);
        if (!$senderCardValidator)
        {
            return ApiResponse::error('sender card in not valid', 400);
        }
        $card = $this->cardService->getCardWithCardNumber($data['sender']);
        $result = $this->otpService->sendOtp($data['sender'],$data['amount'], $card->account->user->mobile);

        return ApiResponse::success($result);

    }
}
