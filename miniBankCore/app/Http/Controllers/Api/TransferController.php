<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Manager\CardValidator;
use App\Repositories\TransferRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransferController extends Controller
{

    protected $transferService;

    public function __construct(TransferRepository $transferRepository)
    {
        $this->transferService = $transferRepository;
    }

    public function transfer(Request $request)
    {
        $data = $request->all();



        $validator = Validator::make($data, [
            'sender' => 'required|string',
            'receiver' => 'required|string',
            'amount' => 'required|integer|between:50000,500000000',
            'cvv2' => 'required|string',
//            'expireDate' => 'required|',
            'otp' => 'required|integer',
        ]);
        $data['sender'] = convertNumber($data['sender']);
        $data['receiver'] = convertNumber($data['receiver']);
        $data['amount'] = convertNumber($data['amount']);
        $data['cvv2'] = convertNumber($data['cvv2']);
        $data['otp'] = convertNumber($data['otp']);




        if ($validator->fails()) {
            return ApiResponse::error($validator->errors(), 400);
        }

        $senderCardValidator = CardValidator::getInstance()->validate($data['sender']);
        $receiverCardValidator = CardValidator::getInstance()->validate($data['receiver']);

        if (!$senderCardValidator)
        {
            return ApiResponse::error('sender card in not valid', 400);
        }
        if (!$receiverCardValidator)
        {
            return ApiResponse::error('receiver card in not valid', 400);
        }

        $result = $this->transferService->transfer($data);

        return ApiResponse::success($result);

    }

    public function getLastTransactions($count)
    {
        return ApiResponse::success($this->transferService->getLastTransfers($count));
    }
}
