<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesTable extends Migration
{
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->id();
            $table->string('card_number')->index();
            $table->unsignedBigInteger('amount');
            $table->unsignedBigInteger('transferId')->index();
            $table->timestamps();

            $table->foreign('transferId')->references('id')->on('transfers');

        });
    }


    public function down(): void
    {
        Schema::dropIfExists('fees');
    }
}
