<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('card_number')->index();
            $table->bigInteger('amount');
            $table->unsignedBigInteger('transferId')->index();
            $table->timestamps();

            $table->foreign('transferId')->references('id')->on('transfers');

        });
    }


    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
}
