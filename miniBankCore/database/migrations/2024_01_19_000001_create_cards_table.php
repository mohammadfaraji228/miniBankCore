<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_id')->index();
            $table->string('card_number')->unique();
            $table->date('expiration_date');
            $table->string('cvv2');
            $table->timestamps();

            $table->foreign('account_id')->references('id')->on('account');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('cards');
    }
}
