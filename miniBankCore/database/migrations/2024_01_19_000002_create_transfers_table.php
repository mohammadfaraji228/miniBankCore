<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->id();
            $table->string('sender_card_number')->index();
            $table->string('receiver_card_number')->index();
            $table->bigInteger('amount');
            $table->bigInteger('fee');
            $table->timestamp('transfer_date')->useCurrent();
            $table->timestamps();
        });
    }


    public function down(): void
    {
        Schema::dropIfExists('transfers');
    }
}
