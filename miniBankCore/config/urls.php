<?php

return [
    'binance' => [
        'price' => env('BINANCE_PRICE_URL', 'https://api.binance.com/api/v3/ticker/price')
    ],
    'trader' => [
        'submitOrder' => env('TRADER_SUBMIT_ORDER_URL' ,'127.0.0.1:8001/trader/order'),

        'cancelOrder' => env('TRADER_CANCEL_ORDER_URL' ,'127.0.0.1:8001/trader/cancel'),

        'getOrderStatus' => env('TRADER_GET_ORDER_STATUS_URL','127.0.0.1:8001/trader/status'),

        'getOpenOrders' => env('TRADER_GET_OPEN_ORDERS_URL','127.0.0.1:8001/trader/open_orders'),

        'getBalance' => env('TRADER_GET_BALANCE_URL','127.0.0.1:8001/trader/balance'),

        'encryptData' => env('TRADER_ENCRYPT_URL','127.0.0.1:8001/trader/encrypt')

    ],

];

